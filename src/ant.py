import torch
import random as rn
import numpy as np

# The class representing a single ant
class Ant:
    def __init__(self, graph, current_node):
        self.graph = graph
        self.current_node = current_node
        self.memory = [self.current_node[0]]

        self.alpha = 3
        self.beta = 1

        self.den_weight = 0

    def start_exploration(self):
        while True:
            if(len(self.memory) >= len(self.graph.nodes)):
                break

            print("Current node: {0}".format(self.current_node[0]))
            self.go_next_node()
    
    def go_next_node(self):
        next_node = self._get_next_node(self.current_node)
        self.memory.append(next_node[0])
        self.current_node = next_node

    def _get_next_node(self, node):
        probabilities = []
        total_weights = 0

        # Calculate the denominator in the main formula
        for city in self.graph.nodes:
            if(city[0] in self.memory):
                continue
                
            distance = self.graph.nodes_distance(node, city)
            pheromone = self.graph.get_pheromone(node, city)

            total_weights += (pheromone ** self.alpha) * ((1 / distance)  ** self.beta)

        # Calculate the probabilities
        for city in self.graph.nodes:
            if(city[0] in self.memory):
                probabilities.append(0)
                continue

            distance = self.graph.nodes_distance(node, city)
            pheromone = self.graph.get_pheromone(node, city)

            probability = (pheromone ** self.alpha) * ((1 / distance) ** self.beta)
            probability = probability / total_weights
            probabilities.append(probability)

        # Getting the next node using probabilities
        idx = np.random.choice(len(self.graph.nodes), p = probabilities)
        return self.graph.nodes[idx]