import csv
import torch
import random as rn
from numpy import linalg

# The class representing the main graph
class Graph:
    def __init__(self, dataset_path):
        self.cuda = torch.device('cuda')
        self.dataset_path = dataset_path

        self.nodes = torch.tensor(self._parse_csv(dataset_path), dtype = torch.int32, device = self.cuda)
        self.edges = torch.tensor([], dtype = torch.int32, device = self.cuda)

        # Default pheromone value
        self.default_pheromone = 1


    def nodes_distance(self, n1, n2):
        _, x1, y1 = n1
        _, x2, y2 = n2

        return self.points_distance(x1, y1, x2, y2)

    def points_distance(self, x1, y1, x2, y2):
        return linalg.norm([[x1, y1], [x2, y2]])

    def add_edge(self, node_a, node_b, pheromone = 0):
        new_edge = torch.tensor([[node_a[0], node_b[0], pheromone]], dtype = torch.int32, device = self.cuda)
        self.edges = torch.cat([self.edges, new_edge])

        return self.edges.size()[0]

    def get_pheromone(self, node_a, node_b):
        res = self.default_pheromone

        for x in self.edges:
            if x[0] == node_a[0] and x[1] == node_b[0]:
                res = x[2]

        return res

    def update_pheromone(self, node_a, node_b, new_pheromone):
        for val in self.edges:
            if(val[0] == node_a[0] and val[1] == node_b[0]):
                val[2] = new_pheromone

    def get_random_node(self):
        return rn.choice(self.nodes)

    def _parse_csv(self, path, skip_header = True):
        with open(path) as f:
            data = []
            reader = csv.reader(f)

            if(skip_header):
                next(reader)
            
            for r in reader:
                # Transform to int
                r = list(map(lambda x: int(x), r))

                data.append(r)

        return data    