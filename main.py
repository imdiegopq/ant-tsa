from src.graph import Graph
from src.ant import Ant

gr = Graph("dataset/santa_cities.csv")

ant1 = Ant(gr, gr.get_random_node())
ant1.start_exploration()